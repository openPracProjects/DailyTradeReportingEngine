package com.proj.fx.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.activation.UnsupportedDataTypeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.MessageTransformationException;
import org.springframework.messaging.Message;
import org.springframework.util.StringUtils;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.Currency;
import com.proj.fx.domain.Entity;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.domain.FlowDirection;
import com.proj.fx.domain.Instruction;
import com.proj.fx.service.CurrencyService;
import com.proj.fx.service.EntityService;
import com.proj.fx.service.InstructionService;
import com.proj.fx.service.ReportService;
import com.proj.fx.util.LogUtil;
import com.proj.fx.util.ProjectUtil;

@MessageEndpoint
public class InstrReqProcessTransformer {
	
	@Autowired
	private EntityService entityService;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	private InstructionService instructionService;
	
	@Autowired
	private ReportService reportService;
	
	@Transformer
	public Message<?> transform(Message<?> message) throws MessageTransformationException {	
		Message<?> returnMessage = null;
		String fileContent = (String) message.getPayload();
		String instrMsgs[] = fileContent.split("\\r?\\n");
		if(instrMsgs.length>0) {
			List<String> failedMessages = processInstructions(instrMsgs);
			returnMessage = MessageBuilder.withPayload(failedMessages).copyHeadersIfAbsent(message.getHeaders()).build();			
		}
		return returnMessage;
	}
	
	private List<String> processInstructions(String[] instrMsgs){
		List<String> failedInsructions = new ArrayList<>();
		if(instrMsgs.length>0) {
			for(String instrMsg : instrMsgs) {
				String[] instrElements = instrMsg.split(",");
				try {
					if(null != instrElements) {
						String entityName = instrElements[0];
						if(entityName.equals(InstructionConstants.REPORT_INSTRUCTION)) {
							if(instrElements.length==2) {
								String dateString = instrElements[1];
								Date reportDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.parse(dateString);
								reportService.generateReport(reportDate);
							}
						}else {
							if(instrElements.length==8) {
								String buySell = instrElements[1];
								String agreedFxValue = instrElements[2];
								String currencyAcronym = instrElements[3];
								String instrDateValue = instrElements[4];
								String settDateValue = instrElements[5];
								String unitsValue = instrElements[6];
								String pricePerUnitValue = instrElements[7];
								
								Entity entity = entityService.findEntity(entityName);
								if(null == entity) {
									throw new UnsupportedDataTypeException(String.format("Entity name  [%s] in instruction is not valid",entityName));
								}
								
								FlowDirection flowDirection = FlowDirection.findByAbbr(buySell);
								if(null == flowDirection) {
									throw new UnsupportedDataTypeException(String.format("FlowDirection in instruction [%s] is not valid",buySell));
								}
								
								BigDecimal agreedFx = null;
								
								if(!StringUtils.isEmpty(agreedFxValue)) {
									agreedFx = new BigDecimal(agreedFxValue);
								}else {
									throw new UnsupportedDataTypeException("Data is not correct for Agreed Fx value");
								}
								
								Currency currency = currencyService.findCurrency(currencyAcronym);
								if(null == currency) {
									throw new UnsupportedDataTypeException(String.format("Currency mentioned in instruction [%s] is not valid",currencyAcronym));
								}else if(!currency.isEnabled()) {
									throw new UnsupportedDataTypeException(String.format("Currency mentioned in instruction [%s] is not enabled",currencyAcronym));
								}
								
								Date instructionDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.parse(instrDateValue);
								Date settlementDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.parse(settDateValue);
								long units = 0;

								if(!StringUtils.isEmpty(unitsValue)) {
									units = Long.parseLong(unitsValue);
								}else {
									throw new UnsupportedDataTypeException("Data is not correct for Units");
								}
								
								BigDecimal pricePerUnit = null;
								
								if(!StringUtils.isEmpty(pricePerUnitValue)) {
									pricePerUnit = new BigDecimal(pricePerUnitValue);
								}else {
									throw new UnsupportedDataTypeException("Data is not correct for Price Per Unit");
								}
								
								Instruction instruction = new Instruction();
								instruction.setEntity(entity);
								instruction.setFlowDirection(flowDirection);
								instruction.setAgreedFx(agreedFx);
								instruction.setTxCurrency(currency);
								instruction.setInstructionDate(instructionDate);
								instruction.setSettlementDate(settlementDate);
								instruction.setUnits(units);
								instruction.setPerUnitPrice(pricePerUnit);
								boolean isDateChanged = ProjectUtil.handleSettlementDate(instruction);
								if(isDateChanged) {
									LogUtil.Log(ErrorCodes.INFO, String.format("Settlement Date has been changed for insruction message [%s] ", instrMsg));
								}
								boolean isSaved = instructionService.saveInstructionDetail(instruction);
								if(!isSaved) {
									LogUtil.Log(ErrorCodes.ERROR, String.format("Error Occured while saving instruction : %s", instruction));
								}
							}else {
								LogUtil.Log(ErrorCodes.ERROR, String.format("Instruction Received does not contains all necessary elements : %s", instrMsg));
							}
						}
					}
				} catch (Exception e) {
					failedInsructions.add(instrMsg);
					LogUtil.Log(ErrorCodes.ERROR, String.format("Error occured while processing instruction message : %s, error description : %s", instrMsg, e.getMessage()));
				}
			}
		}
		return failedInsructions;
	}
}