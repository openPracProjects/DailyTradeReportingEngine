package com.proj.fx.domain;

public enum FlowDirection {

	BUY("B", "Outgoing", "Buy"),
	SELL("S", "Incoming", "Sell");
	
	private String value;
	private String flowDirection;
	private String description;
	
	private FlowDirection(String value, String flowDirection, String description) {
		this.value = value;
		this.flowDirection = flowDirection;
		this.description = description;
	}

	public static FlowDirection findByAbbr(String abbr){
	    for(FlowDirection fd : values()){
	        if( fd.getValue().equals(abbr)){
	            return fd;
	        }
	    }
	    return null;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFlowDirection() {
		return flowDirection;
	}

	public void setFlowDirection(String flowDirection) {
		this.flowDirection = flowDirection;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
