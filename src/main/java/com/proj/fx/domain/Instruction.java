package com.proj.fx.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.proj.fx.constants.InstructionConstants;

public class Instruction {

	private Entity entity;
	private FlowDirection flowDirection;
	private Currency txCurrency;
	private Date instructionDate;
	private Date settlementDate;
	private BigDecimal agreedFx;
	private long units;
	private BigDecimal perUnitPrice;
	
	public Entity getEntity() {
		return entity;
	}
	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	public FlowDirection getFlowDirection() {
		return flowDirection;
	}
	public void setFlowDirection(FlowDirection flowDirection) {
		this.flowDirection = flowDirection;
	}
	public Currency getTxCurrency() {
		return txCurrency;
	}
	public void setTxCurrency(Currency txCurrency) {
		this.txCurrency = txCurrency;
	}
	public Date getInstructionDate() {
		return instructionDate;
	}
	public void setInstructionDate(Date instructionDate) {
		this.instructionDate = instructionDate;
	}
	public Date getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}
	public BigDecimal getAgreedFx() {
		return agreedFx;
	}
	public void setAgreedFx(BigDecimal agreedFx) {
		this.agreedFx = agreedFx;
	}
	public long getUnits() {
		return units;
	}
	public void setUnits(long units) {
		this.units = units;
	}
	public BigDecimal getPerUnitPrice() {
		return perUnitPrice;
	}
	public void setPerUnitPrice(BigDecimal perUnitPrice) {
		this.perUnitPrice = perUnitPrice;
	}
	
	@Override
	public String toString() {
		String entityAcronym = null;
		if(null != this.entity) {
			entityAcronym = this.entity.getName();
		}
		
		String txCurrAcronym = null;
		if(null != this.txCurrency) {
			txCurrAcronym = this.txCurrency.getAcronym();
		}
		
		String buySell = null;
		if(null != this.flowDirection) {
			buySell = this.flowDirection.getValue();
		}
		
		return entityAcronym+","+buySell+","+this.agreedFx+","+txCurrAcronym+","+
				InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.format(this.instructionDate)+","+
				InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.format(this.settlementDate)+","+this.units+","+this.perUnitPrice;
	}
}
