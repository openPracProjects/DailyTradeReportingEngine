package com.proj.fx.domain;

import java.util.List;

public class Currency {

	private String acronym;
	private String name;
	private int numberOfDecimals;
	private boolean enabled;
	private List<Integer> closingDays;
	
	public Currency(String acronym, String name, int numberOfDecimals, boolean enabled, List<Integer> closingDays) {
		this.acronym = acronym;
		this.name = name;
		this.numberOfDecimals = numberOfDecimals;
		this.enabled = enabled;	
		this.closingDays = closingDays;
	}
	
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumberOfDecimals() {
		return numberOfDecimals;
	}
	public void setNumberOfDecimals(int numberOfDecimals) {
		this.numberOfDecimals = numberOfDecimals;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public List<Integer> getClosingDays() {
		return closingDays;
	}
	public void setClosingDays(List<Integer> closingDays) {
		this.closingDays = closingDays;
	}
}
