package com.proj.fx.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ReportBean {

	BigDecimal incomingTotal;
	BigDecimal outgoingTotal;
	List<String> incomingEntitiesRanking;
	List<String> outgoingEntitiesRanking;
	Date date;
	
	public BigDecimal getIncomingTotal() {
		return incomingTotal;
	}
	public void setIncomingTotal(BigDecimal incomingTotal) {
		this.incomingTotal = incomingTotal;
	}
	public BigDecimal getOutgoingTotal() {
		return outgoingTotal;
	}
	public void setOutgoingTotal(BigDecimal outgoingTotal) {
		this.outgoingTotal = outgoingTotal;
	}
	public List<String> getIncomingEntitiesRanking() {
		return incomingEntitiesRanking;
	}
	public void setIncomingEntitiesRanking(List<String> incomingEntitiesRanking) {
		this.incomingEntitiesRanking = incomingEntitiesRanking;
	}
	public List<String> getOutgoingEntitiesRanking() {
		return outgoingEntitiesRanking;
	}
	public void setOutgoingEntitiesRanking(List<String> outgoingEntitiesRanking) {
		this.outgoingEntitiesRanking = outgoingEntitiesRanking;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
