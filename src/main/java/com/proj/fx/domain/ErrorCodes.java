package com.proj.fx.domain;

public enum ErrorCodes {
	ERROR("ERROR"),
	WARN("WARN"),
	INFO("INFO");
	
	private ErrorCodes(String value) {
		this.value = value;
	}
	
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
