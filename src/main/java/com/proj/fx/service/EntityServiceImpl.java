package com.proj.fx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.fx.dao.EntityDao;
import com.proj.fx.domain.Entity;

@Component("entityService")
public class EntityServiceImpl implements EntityService{

	@Autowired
	private EntityDao entityDao;
	
	@Override
	public List<Entity> getAllEntities() {
		return entityDao.getAllEntities();
	}

	@Override
	public Entity findEntity(String entityName) {
		return entityDao.findEntity(entityName);
	}
	

}
