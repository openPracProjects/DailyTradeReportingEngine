package com.proj.fx.service;

import java.util.List;

import com.proj.fx.domain.Currency;

public interface CurrencyService {

	List<Currency> getAllCurrencies();
	
	Currency findCurrency(String currencyAcronym);
}
