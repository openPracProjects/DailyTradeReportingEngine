package com.proj.fx.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.domain.FlowDirection;
import com.proj.fx.domain.Instruction;
import com.proj.fx.domain.ReportBean;
import com.proj.fx.util.LogUtil;

@Component("reportService")
public class ReportServiceImpl implements ReportService {

	@Autowired
	private InstructionService instructionService;
	
	@Value("${FX.INSTRUCTION.REPORT.ENTITY.DISPLAY.LIMIT}")
	private int entityDisplayLimit;
	
	@Override
	public ReportBean generateReport(Date date) {
		ReportBean reportBean = null;
		String instructionDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.format(date);
		List<Instruction> instructions = instructionService.queryInstructionsByDate(date);
		if(null != instructions && !instructions.isEmpty()) {
			reportBean = new ReportBean();
			BigDecimal incomingUSDTotal = BigDecimal.ZERO;
			BigDecimal outgoingUSDTotal = BigDecimal.ZERO;
			
			Map<String,BigDecimal> entityMapIncoming = new HashMap<>();
			Map<String,BigDecimal> entityMapOutgoing = new HashMap<>();
			
			for(Instruction instruction : instructions) {
				BigDecimal usdEq = instruction.getAgreedFx().multiply(instruction.getPerUnitPrice().multiply(new BigDecimal(instruction.getUnits())));
				if(instruction.getFlowDirection().equals(FlowDirection.BUY)) {
					outgoingUSDTotal = outgoingUSDTotal.add(usdEq);
					populateEntityMap(entityMapOutgoing, instruction, usdEq);
				}else {
					incomingUSDTotal = incomingUSDTotal.add(usdEq);
					populateEntityMap(entityMapIncoming, instruction, usdEq);
				}
			}
			
			List<String> sortedIncomingEntities = entityMapIncoming.entrySet()
														           .stream()
														           .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())) // custom Comparator
														           .map(e -> e.getKey()).limit(entityDisplayLimit)
														           .collect(Collectors.toList());
			List<String> sortedOutgoingEntities = entityMapOutgoing.entrySet()
														           .stream()
														           .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())) // custom Comparator
														           .map(e -> e.getKey()).limit(entityDisplayLimit)
														           .collect(Collectors.toList());
			
			reportBean.setDate(date);
			reportBean.setIncomingEntitiesRanking(sortedIncomingEntities);
			reportBean.setOutgoingEntitiesRanking(sortedOutgoingEntities);
			reportBean.setIncomingTotal(incomingUSDTotal);
			reportBean.setOutgoingTotal(outgoingUSDTotal);
			printReport(reportBean,instructionDate);
			//LogUtil.Log(ErrorCodes.INFO, String.format("## REPORT ## For Date [%s] , Equivalent USD Icoming is [%s] and Outgoing is [%s]",instructionDate,incomingUSDTotal.toString(), outgoingUSDTotal.toString()));
		}else {
			LogUtil.Log(ErrorCodes.WARN, String.format("Not able to print Report, as there are no Instructions received for Date : %s",instructionDate));
		}
		return reportBean;
	}

	private void populateEntityMap(Map<String, BigDecimal> entityMapOutgoing, Instruction instruction,
			BigDecimal usdEq) {
		BigDecimal entityTotal;
		entityTotal = entityMapOutgoing.get(instruction.getEntity().getName());
		if(null == entityTotal) {
			entityTotal = BigDecimal.ZERO;
		}
		entityMapOutgoing.put(instruction.getEntity().getName(), entityTotal.add(usdEq));
	}

	private void printReport(ReportBean reportBean, String instructionDate) {
		if(null != reportBean) {
			LogUtil.Log(ErrorCodes.INFO, String.format("---------------------------- Report for Date [%s] ----------------------------",instructionDate));
			LogUtil.Log(ErrorCodes.INFO, String.format("Total Incoming total (is USD) : %s",reportBean.getIncomingTotal().toPlainString()));
			LogUtil.Log(ErrorCodes.INFO, String.format("Total Outgoing total (is USD) : %s",reportBean.getOutgoingTotal().toPlainString()));
			LogUtil.Log(ErrorCodes.INFO, String.format("Entity Ranking for Incoming (in descending order) : %s",reportBean.getIncomingEntitiesRanking()));
			LogUtil.Log(ErrorCodes.INFO, String.format("Entity Ranking for Outgoing (in descending order) : %s",reportBean.getOutgoingEntitiesRanking()));
		}else {
			LogUtil.Log(ErrorCodes.WARN, String.format("Not able to print Report, as there are no Instructions received for Date : %s",instructionDate));
		}
	}
	
}
