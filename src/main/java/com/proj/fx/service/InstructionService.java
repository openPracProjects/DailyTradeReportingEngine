package com.proj.fx.service;

import java.util.Date;
import java.util.List;

import com.proj.fx.domain.Instruction;

public interface InstructionService {

	boolean saveInstructionDetail(Instruction instruction);
	
	List<Instruction> queryInstructionsByDate(Date instructionDate);
}
