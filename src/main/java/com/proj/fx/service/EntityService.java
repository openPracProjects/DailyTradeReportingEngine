package com.proj.fx.service;

import java.util.List;

import com.proj.fx.domain.Entity;

public interface EntityService {

	List<Entity> getAllEntities();
	
	Entity findEntity(String entityName);
}
