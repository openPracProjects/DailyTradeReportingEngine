package com.proj.fx.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.fx.dao.InstructionDAO;
import com.proj.fx.domain.Instruction;

@Component("instructionService")
public class InstructionServiceImpl implements InstructionService{

	@Autowired
	private InstructionDAO instructionDAO;
	
	@Override
	public boolean saveInstructionDetail(Instruction instruction) {
		return instructionDAO.saveInstructionDetail(instruction);
	}

	@Override
	public List<Instruction> queryInstructionsByDate(Date instructionDate) {
		return instructionDAO.queryInstructionsByDate(instructionDate);
	}

}
