package com.proj.fx.service;

import java.util.Date;

import com.proj.fx.domain.ReportBean;

public interface ReportService {

	ReportBean generateReport(Date date);
	
}
