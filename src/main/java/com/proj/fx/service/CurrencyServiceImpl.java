package com.proj.fx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proj.fx.dao.CurrencyDao;
import com.proj.fx.domain.Currency;

@Component("currencyService")
public class CurrencyServiceImpl implements CurrencyService{

	@Autowired
	private CurrencyDao currencyDao;
	
	@Override
	public List<Currency> getAllCurrencies() {
		return currencyDao.getAllCurrencies();
	}

	@Override
	public Currency findCurrency(String currencyAcronym) {
		return currencyDao.findCurrency(currencyAcronym);
	}
}
