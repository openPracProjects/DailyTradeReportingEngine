package com.proj.fx.constants;

import java.text.SimpleDateFormat;

public class InstructionConstants {

	public static final String ENTITY = "entity";
	public static final String BUY_SELL_INDICATOR = "BuySellIndicator";
	public static final String AGREED_FX = "AgreedFx";
	public static final String CURRENCY = "Currency";
	public static final String INSTRUCTION_DATE = "InstructionDate";
	public static final String SETTLEMENT_DATE = "SettlementDate";
	public static final String UNITS = "Units";
	public static final String PRICE_PER_UNIT = "PricePerUnit";
	public static final SimpleDateFormat DATE_FORMAT_yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	public static final SimpleDateFormat DATE_FORMAT_dd_MMM_yyyy = new SimpleDateFormat("dd MMM yyyy");
	
	public static final String ERROR = "ERROR";
	public static final String INFO = "INFO";
	public static final String WARN = "WARN";
	public static final String FATAL = "FATAL";
	
	public static final String REPORT_INSTRUCTION = "REPORT";
}
