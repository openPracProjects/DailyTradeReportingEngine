package com.proj.fx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:/context/application.xml")
public class DailyTradeReportingEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyTradeReportingEngineApplication.class, args);
	}
}
