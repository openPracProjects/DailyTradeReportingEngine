package com.proj.fx.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.proj.fx.domain.Entity;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.util.LogUtil;

@Component
public class EntityDao {

	/**
	 * This method returns predefined list of Entities
	 * Ideally these entities should come from Database 
	 * @return
	 */
	public List<Entity> getAllEntities() {

		List<Entity> enities = new ArrayList<>();
		enities.add(new Entity("1", "Foo", "foo", "foo Entity"));
		enities.add(new Entity("2", "Bar", "bar", "Bar Entity"));
		enities.add(new Entity("3", "Abc", "abc", "Abc Entity"));

		return enities;
	}

	/**
	 * This method gives Entity Object if 'entityName' passed in method
	 * is registered with Trade Reporting Engine
	 * @param entityName
	 * @return Entity Object as registered with Trade Reporting Engine
	 */
	public Entity findEntity(String entityName) {
		Entity entity = null;
		try {
			entity =  this.getAllEntities().stream().filter(e -> e.getName().equalsIgnoreCase(entityName)).findFirst().get();
		} catch (Exception e) {
			LogUtil.Log(ErrorCodes.ERROR, String.format("Entity is not recognized [%s]", entityName));
		}
		return entity;
	}
}
