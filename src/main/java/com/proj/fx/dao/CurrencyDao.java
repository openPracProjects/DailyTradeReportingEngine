package com.proj.fx.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Component;

import com.proj.fx.domain.Currency;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.util.LogUtil;

@Component
public class CurrencyDao {

	/**
	 * This method returns predefined list of currencies
	 * Ideally these currencies should come from Database 
	 * @return
	 */
	public List<Currency> getAllCurrencies() {

		List<Currency> currencies = new ArrayList<>();
		List<Integer> satSunClosing = new ArrayList<>(Arrays.asList(Calendar.SATURDAY,Calendar.SUNDAY));
		List<Integer> friSatClosing = new ArrayList<>(Arrays.asList(Calendar.FRIDAY,Calendar.SATURDAY));
		
		currencies.add(new Currency("USD", "United States Dollar", 5, true,satSunClosing));
		currencies.add(new Currency("AED", "United Arab Emirates Dirham", 5, true,friSatClosing));
		currencies.add(new Currency("SAR", "Saudi Arabia Riyal", 5, true,friSatClosing));
		currencies.add(new Currency("JPY", "Japan Yen", 5, true,satSunClosing));
		currencies.add(new Currency("SGD", "Singapore Dollar", 5, true,satSunClosing));
		currencies.add(new Currency("INR", "Indian Rupee", 5, true,satSunClosing));
		currencies.add(new Currency("GBP", "British Pound", 5, true,satSunClosing));
		currencies.add(new Currency("CUP", "Cuban Peso", 5, false,satSunClosing));

		return currencies;
	}

	/**
	 * This method gives Currency Object if 'currencyAcronym' passed in method
	 * is supported by the Trade Reporting Engine
	 * @param currencyAcronym
	 * @return Currency Object as registered with Trade Reporting Engine
	 */
	public Currency findCurrency(String currencyAcronym) {
		Currency currency = null;
		try {
			currency =  this.getAllCurrencies().stream().filter(c -> c.getAcronym().equals(currencyAcronym)).findFirst().get();
		} catch (Exception e) {
			LogUtil.Log(ErrorCodes.ERROR, String.format("Currency is not recognized [%s]", currencyAcronym));
		}
		return currency;
		
	}
}
