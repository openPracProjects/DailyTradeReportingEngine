package com.proj.fx.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.domain.Instruction;
import com.proj.fx.util.LogUtil;

@Component
public class InstructionDAO {
	
	private Map<String , List<Instruction>> instructionStore = new HashMap<String , List<Instruction>>();
	
	public boolean saveInstructionDetail(Instruction instruction) {
		boolean isSaved = false;
		
		String instructionDate = InstructionConstants.DATE_FORMAT_yyyyMMdd.format(instruction.getInstructionDate());
		
		List<Instruction> instructions = instructionStore.get(instructionDate);
		if(null == instructions) {
			instructions = new ArrayList<>();
		}
		instructions.add(instruction);
		instructionStore.put(instructionDate, instructions);
		isSaved = true;
		LogUtil.Log(ErrorCodes.INFO, String.format("Instruction saved successfully in DB : %s", instruction));
		return isSaved;
	}
	
	public List<Instruction> queryInstructionsByDate(Date instructionDate) {
		List<Instruction> instructions = null;
		String instructionDateFormatted = InstructionConstants.DATE_FORMAT_yyyyMMdd.format(instructionDate);
		if(null != instructionStore) {
			instructions = instructionStore.get(instructionDateFormatted);
		}
		return instructions;
	}
}
