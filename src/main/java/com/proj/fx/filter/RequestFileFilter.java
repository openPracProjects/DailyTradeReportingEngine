package com.proj.fx.filter;

import org.springframework.integration.annotation.Filter;
import org.springframework.integration.core.MessageSelector;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.util.LogUtil;

@Component
public class RequestFileFilter implements MessageSelector{

	@Filter
	@Override
	public boolean accept(Message<?> message) {
		boolean accept = false;
		String fileContent = (String) message.getPayload();
		if(!StringUtils.isEmpty(fileContent)) {
			accept = true;
		}else {
			LogUtil.Log(ErrorCodes.ERROR, "Request file contains no instructions, discarding request file.");
		}
		return accept;
	}

}
