package com.proj.fx.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.proj.fx.domain.Instruction;

public class ProjectUtil {
	
	public static boolean handleSettlementDate(Instruction instruction) {
		boolean isChanged = false;
		Date settlementDate = instruction.getSettlementDate();
		Calendar cal = new GregorianCalendar();
	    cal.setTime(settlementDate);
	    
		if(instruction.getTxCurrency().getClosingDays().contains(Calendar.FRIDAY)) {
			if(Calendar.FRIDAY == cal.get(Calendar.DAY_OF_WEEK)) {
				cal.add(Calendar.DATE, 2);
				settlementDate = cal.getTime();
				isChanged = true;
			}else if(Calendar.SATURDAY == cal.get(Calendar.DAY_OF_WEEK)){
				cal.add(Calendar.DATE, 1);
				settlementDate = cal.getTime();
				isChanged = true;
			}
		}else {
			if(Calendar.SATURDAY == cal.get(Calendar.DAY_OF_WEEK)) {
				cal.add(Calendar.DATE, 2);
				settlementDate = cal.getTime();
				isChanged = true;
			}else if(Calendar.SUNDAY == cal.get(Calendar.DAY_OF_WEEK)){
				cal.add(Calendar.DATE, 1);
				settlementDate = cal.getTime();
				isChanged = true;
			}
		}
		if(isChanged) {
			instruction.setSettlementDate(settlementDate);
		}
		return isChanged;
	}
}
