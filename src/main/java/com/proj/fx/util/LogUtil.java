package com.proj.fx.util;

import java.util.Date;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.ErrorCodes;

public class LogUtil {

	public static void Log(ErrorCodes errorCodes , String message) {
		Date date = new Date();
		String currentDateTime = InstructionConstants.DATE_TIME_FORMAT.format(date);
		String formattedMsg = currentDateTime.concat(String.format("   [%s]   :   %s", errorCodes.getValue(), message));
		switch (errorCodes) {
		case ERROR:
			System.out.println(formattedMsg);
			break;
		default:
			System.err.println(formattedMsg);
			break;
		}
		
	}
}
