package com.proj.fx.serviceactivator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.util.LogUtil;

@Component
@MessageEndpoint
public class InstrMsgAfterCareServiceActivator {
	
	@Value("${FX.INSTRUCTION.REQUEST.FILE.ERROR.DIRECTORY}")
	private String errorFileDirectory;
	
	@Value("${FX.INSTRUCTION.REQUEST.FILE.ERROR.FILE.NAME}")
	private String errorFileName;
	
	@ServiceActivator
	public void afterCare(Message<?> message) {
		@SuppressWarnings("unchecked")
		List<String> failedMessages = (List<String>) message.getPayload();
		if(null != failedMessages && !failedMessages.isEmpty()) {
			File errorDirectory = new File(errorFileDirectory);
			if(!errorDirectory.exists()) {
				errorDirectory.mkdir();
			}
			String dateTimeStamp = new java.text.SimpleDateFormat("yyyyMMdd_HH-mm-ss-SSS").format(new java.util.Date());
			String fullFilePath = errorFileDirectory.concat(errorFileName).concat(dateTimeStamp).concat(".csv");
			try(FileWriter fileWriter = new FileWriter(fullFilePath)) {
				PrintWriter printWriter = new PrintWriter(fileWriter);
				for(String instr : failedMessages) {
					printWriter.println(instr);
				}
				printWriter.close();
			} catch (IOException e) {
				LogUtil.Log(ErrorCodes.ERROR, e.getMessage());
			}
		}
	}
}
