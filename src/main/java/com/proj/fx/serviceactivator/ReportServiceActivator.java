package com.proj.fx.serviceactivator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.ErrorCodes;
import com.proj.fx.domain.FlowDirection;
import com.proj.fx.domain.Instruction;
import com.proj.fx.service.InstructionService;
import com.proj.fx.util.LogUtil;

@Component
@MessageEndpoint
public class ReportServiceActivator {
	
	@Autowired
	private InstructionService instructionService;

	@ServiceActivator
	public void logDailyReport(Message<?> message) {
		Date date = new Date();
		String instructionDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.format(date);
		List<Instruction> instructions = instructionService.queryInstructionsByDate(date);
		if(null != instructions) {
			BigDecimal incomingUSDTotal = BigDecimal.ZERO;
			BigDecimal outgoingUSDTotal = BigDecimal.ZERO;
			
			for(Instruction instruction : instructions) {
				BigDecimal usdEq = null;
				if(instruction.getFlowDirection().equals(FlowDirection.BUY)) {
					usdEq = instruction.getAgreedFx().multiply(instruction.getPerUnitPrice().multiply(new BigDecimal(instruction.getUnits())));
					outgoingUSDTotal = outgoingUSDTotal.add(usdEq);
				}else {
					usdEq = instruction.getAgreedFx().multiply(instruction.getPerUnitPrice().multiply(new BigDecimal(instruction.getUnits())));
					incomingUSDTotal = incomingUSDTotal.add(usdEq);
				}
			}
			LogUtil.Log(ErrorCodes.INFO, String.format("For Date [%s] , Equivalent USD Icoming is [%s] and Outgoing is [%s]",instructionDate,incomingUSDTotal.toString(), outgoingUSDTotal.toString()));
		}
	}
}
