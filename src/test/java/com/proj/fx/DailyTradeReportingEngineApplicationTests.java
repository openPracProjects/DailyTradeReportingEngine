package com.proj.fx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:/context/application-test.xml")
public class DailyTradeReportingEngineApplicationTests {		
	
    public static void main(String[] args) throws Exception {
        SpringApplication.run(DailyTradeReportingEngineApplicationTests.class, args);
    } 
    
}