package com.proj.fx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.proj.fx.constants.InstructionConstants;
import com.proj.fx.domain.Instruction;
import com.proj.fx.domain.ReportBean;
import com.proj.fx.service.InstructionService;
import com.proj.fx.service.ReportService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DailyTradeReportingEngineApplicationTests.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class InsructionRequestMessageTest {

	private static final String testFileLocation = "sample/FX_INSTRUCTION_REQUEST_TEST_001.csv";
	
	@Value("${FX.INSTRUCTION.REQUEST.DIRECTORY}")
	private String instrFileDirectory;
	
	@Value("${FX.INSTRUCTION.REQUEST.FILE.ERROR.FILE.NAME}")
	private String inputFileName;
	
	@Autowired
	private InstructionService instructionService;
	
	@Autowired
	private ReportService reportService;
	
	@Test
	public void testInsructionRequestMsg() throws Exception {
		String testFileDate = "28 Aug 2018";
		Date testDate = InstructionConstants.DATE_FORMAT_dd_MMM_yyyy.parse(testFileDate);
		
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		File testFile = new File(classLoader.getResource(testFileLocation).getFile());
		
		File dest = new File(instrFileDirectory.concat(inputFileName).concat(testDate.getTime()+"").concat(".csv"));
		FileUtils.copyFile(testFile, dest);
		Thread.sleep(30000); // Wait time to let all the instructions processed
		List<Instruction> savedInstructions = instructionService.queryInstructionsByDate(testDate);
		dest.delete();
		assertNotNull(savedInstructions);
		assertTrue(savedInstructions.size()==5);
		ReportBean reportBean= reportService.generateReport(testDate);
		assertNotNull(reportBean);
		assertEquals(reportBean.getIncomingTotal().toString(), "22387.0000");
		assertEquals(reportBean.getOutgoingTotal().toString(), "50432.0000");
		assertEquals(reportBean.getIncomingEntitiesRanking().toString(), "[Bar, Foo]");
		assertEquals(reportBean.getOutgoingEntitiesRanking().toString(), "[Abc, Bar, Foo]");
	}
}
