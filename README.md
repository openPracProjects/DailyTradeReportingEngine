# DailyTradeReportingEngine

This project uses : Maven, Spring Boot, Spring Integration

To build this project : mvn clean install -X

## Description
----------------

The project takes Instructions via input file. The sample file for the same can be found at : src\test\resources\sample\FX_INSTRUCTION_REQUEST_TEST_001.csv

- Following Enitites are only Recognized by the System : foo, bar, abc
- Following Currencies are only Recognized by the System : USD, AED, SAR, JPY, SGD,INR, GBP, CUP
- Apart from these Entities and Currencies, Instruction Message will be rejected

Step to Run :
-------------
- Application is spring boot application the Entry Point for the application is : src\main\java\com\proj\fx\DailyTradeReportingEngineApplication.java
- To run the application use : mvn spring-boot:run
- Once Application is up, Instructions can be given to application in the form of CSV file, the name of the CSV file must be 'FX_INSTRUCTION_REQUEST.csv', and should be placed under folder 'fileShare/fx/instruction/request'
- CSV File is picked every 10 sec, that is configure via poller and can also be changed to a cron expression and externalized to property file
- To view report, Instruction file should have , instruction for viewing report as well. Just like every other instruction in csv file we can provide REPORT, date of report.
- As soon as input csv file is processed its deleted from the source location
- If anything goes wrong during the execution of the Instructions, those Instructions are written to an error file which is location at : fileShare/fx/instruction/error/ 
the format for the Error file is like : FX_INSTRUCTION_REQUEST_20180829_01-46-49-943.csv
Which contains the file name and time stamp when it was created.
- For viewing the cause of error, console can be referred for error description
- If out of 10 instructions only 3 failed, then Error file will contain only 3 instructions
- The Error file can be again moved to 'fileShare/fx/instruction/request' to retry processing, if again failed then it moves back to error folder 'fileShare/fx/instruction/error/'. This way we have exception handler in place, which also gives us an opportunity to retry failed instructions.
- Holiday of any Currency is tied to currency details itself.
- Currency can also be enabled or disabled if required. And if currencies are disabled it won't be processed. Sample test file contains this scenario.
-  No DB has been used all static data setup has been done in "com.proj.fx.dao" package.
- To see testing please refer 'src\test\java\com\proj\fx\InsructionRequestMessageTest.java'
- To run any custom scenario, please Run the application and  provide your instructions in the file 'FX_INSTRUCTION_REQUEST.csv'
and place it under 'fileShare/fx/instruction/request' folder. If you want to see Report at the end of your instructions please add 'REPORT,date for which you want to see report in the format dd MMM yyyy ex . 28 Aug 2018'
- All Project related folder are created automatically, no need to create any project structure.
